# Enalquiler Engineering Technical Test

## 01. FileDownloader

### Your Task

You're given a new task consisting of downloading certain XML from a third-party provider using an already existing component (`FileDownloader` class). When you start with the development, you realize that the code of the component isn't maintainable in its current form. So you decide to refactor the code in order to leave the component really reusable. Your task will be to refactor the code from the component in order to leave it really reusable and maintainable.

### Rules

* The code is located at `FileDownloader.php`
* Focus your refactoring efforts on the `FileDownloader::downloadFile` method. If you want to go further it's up to you.

## 02. Interestellar Spaceship Travel on Mars

### Your Task

You’re part of the team that explores Mars by sending remotely controlled vehicles to the surface of the planet. Develop an API that translates the commands sent from earth to instructions that are understood by the rover.

### Requirements

* You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
* The rover receives a character array of commands.
* Implement commands that move the rover forward/backward (f,b).
* Implement commands that turn the rover left/right (l,r).
* Implement wrapping from one edge of the grid to another. (planets are spheres after all)
* Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point, aborts the sequence and reports the obstacle.

### Example instructions

This is a sample of the instructions that the Rover can receive

    LFFFRFF

### Rules

* Be careful about edge cases and exceptions. We can not afford to lose a mars rover, just because the developers overlooked a null pointer.

## What we will assess about the test?

This will be our guidelines in order to assess this test.

 * **Maintainability**. We want a maintainable solution. A solution that we can change in a future without hurt and pain.
 * **Simplicity**. We believe that a simple solution is also a maintainable solution.

Your solution will be used in a future interview to discuss the design you approach. You don’t have a time limit. Once you’re done with the test send back to us with the results.

Good Luck!