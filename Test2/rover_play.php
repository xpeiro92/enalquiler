<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once "Entities/Rover.php";
include_once "Commands/CommandProcess.php";
include_once "Commands/CommandGetEnvironment2D.php";

echo "hi Rover!<br>";

//---------
//show environment
CommandGetEnvironment2D::simulateEnvironment2D(new Mars());
$environment = CommandGetEnvironment2D::execute();

foreach ($environment[0] as $key => $item) {
    echo "[" . $key . "]";
    foreach ($environment[1] as $key1 => $item1) {
        echo "| ";
        echo $environment[$key][$key1];
        echo " | ";
    }
    echo "<br>";

}
//---------

$stringCommands = "LRBFFFFFRBB";

$rover = Rover::initDefaultRover();

$commandProcess = CommandProcess::defaultCommandProcess();

$commandProcess($rover, $stringCommands);

echo "success!!";
foreach ($rover->getComponents() as $component)
{
    echo "<br>";
    print_r($component);
}

