<?php

include_once "Command.php";
include_once "CommandGetEnvironment2D.php";

class Displace2D implements Command
{
    public static function execute(DisplaceableAndRotateableComponent $component, $displacement = 0)
    {
        $operation_succes = false;

        $environment = CommandGetEnvironment2D::execute();
        $ini_pos = $component->getPosition();
        $degrees = $component->getPointingTo();

//        echo "before";
//        var_dump($ini_pos);
//        echo "<br>-----------<br>";

        $x_displace = round( cos(deg2rad($degrees)) ) * $displacement;
        $y_displace = round( sin(deg2rad($degrees)) ) * $displacement;

        $x_nxt_pos =  ($ini_pos[0] + $x_displace + count($environment)) % count($environment);
        $y_nxt_pos =  ($ini_pos[1] + $y_displace + count($environment)) % count($environment);

        if ( $environment[$x_nxt_pos][$y_nxt_pos] == 0 )
        {
            $component->setPosition([$x_nxt_pos, $y_nxt_pos]);

//            echo "after";
//            var_dump($component->getPosition());
//            echo "<br>-----------<br>";

            if ($component->getPosition() !== $ini_pos )
            {
                $operation_succes = true;
            }
        }

        return $operation_succes;
    }

}