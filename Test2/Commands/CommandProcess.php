<?php

include_once "CommandFactory.php";

class CommandProcess
{
    private $commandFactory;

    public function __construct(CommandFactory $commandFactory)
    {
        $this->commandFactory = $commandFactory;
    }

    public static function defaultCommandProcess()
    {
        $commandFactory = new CommandFactory();

        return new self($commandFactory);
    }

    public function __invoke(Rover $rover, $stringCommands)
    {
        $keys = str_split($stringCommands);

        foreach ($keys as $key)
        {
            $command = $this->commandFactory->__invoke($key, $rover);
            $is_success = $command($rover->getComponentById("displacementLocator"));

            if(!$is_success)
            {
                break;
            }
        }
    }
}