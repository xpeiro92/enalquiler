<?php

include_once "Command.php";
class Rotate implements Command
{
    public static function execute(RotatableComponent $component, $degrees = 0)
    {
        // This must not be like this but I can't develop a real motor either a real locator
        $ini_orientation = $component->getPointingTo();
        $component->setPointingTo($ini_orientation + $degrees);
        $curr_orientation = $component->getPointingTo();

        return ($ini_orientation != $curr_orientation);
    }
}