<?php


class CommandB
{
    private $displace;

    // This should be Motor type but in this case we must to use Locator
    public function __construct(Displace2D $displace)
    {
        $this->displace = $displace;
    }

    public function __invoke(Component $component, $distance = 1)
    {
        return $this->displace::execute($component, ($distance * (-1)) );
    }
}