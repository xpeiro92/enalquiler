<?php

include_once "CommandB.php";
include_once "CommandF.php";
include_once "CommandL.php";
include_once "CommandR.php";
include_once "Rotate.php";
include_once "Displace2D.php";


class CommandFactory
{
    public function __invoke($key, $rover)
    {
        switch ($key)
        {
            case "L":
                $command = new CommandL(new Rotate());
                break;

            case "R":
                $command = new CommandR(new Rotate());
                break;

            case "F":
                $command = new CommandF(new Displace2D());
                break;

            case "B":
                $command = new CommandB(new Displace2D());
                break;

            default:
                break;
        }

        return $command;
    }
}