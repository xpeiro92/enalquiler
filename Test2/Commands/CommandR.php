<?php

class CommandR
{
    private $rotate;

    // This should be Motor type but in this case we must to use Locator
    public function __construct( Rotate $rotate)
    {
        $this->rotate = $rotate;
    }

    public function __invoke(Component $component, $degrees = 90)
    {
        return $this->rotate::execute($component, ($degrees * (-1)) );
    }
}