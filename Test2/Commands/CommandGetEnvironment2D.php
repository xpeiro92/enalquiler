<?php

include_once $_SERVER["DOCUMENT_ROOT"] . "/Test2/Entities/Mars.php";

class CommandGetEnvironment2D
{
    private static $environment2D;

    public static function execute()
    {
        return self::$environment2D->getSurface();
    }

    public static function simulateEnvironment2D($Environment2D)
    {
        self::$environment2D = $Environment2D;
    }

}