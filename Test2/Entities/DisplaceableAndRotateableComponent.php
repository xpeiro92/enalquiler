<?php

include_once "RotatableComponent.php";
interface DisplaceableAndRotateableComponent extends RotatableComponent
{
    public function getPosition();
    public function setPosition($position);
}