<?php

class Mars
{
    private $area;
    private $r;
    private $surface;

    public function __construct( $area = 100 )
    {
        $this->area = $area;
        $this->setR();
        $this->setSurface();
    }

    /**
     * @return int
     */
    public function getArea(): int
    {
        return $this->area;
    }

    /**
     * @param int $area
     */
    public function setArea(int $area)
    {
        $this->area = $area;
        $this->setR();
        $this->setSurface();
    }

    /**
     * @return float|int
     */
    public function getR()
    {
        return $this->r;
    }

    /**
     * @param float|int $r
     */
    private function setR()
    {
        $this->r = ( $this->area / 4*pi() );
    }

    /**
     * @return array
     */
    public function getSurface(): array
    {
        return $this->surface;
    }

    /**
     * @param array $surface
     */
    private function setSurface()
    {
        $side_length = sqrt($this->area);
        $this->surface = array_fill(0, $side_length, 0);
        $this->surface = array_map(function() use ($side_length){return array_fill(0, $side_length, 0);}, $this->surface);

        for ($i = 0; $i < ($this->area / 10); $i++ )
        {
            $this->surface[rand(0, $side_length-1)][rand(0, $side_length-1)] = 1;
        }
    }
}