<?php

interface Component
{
    public function getId();
    public function getStatus();
    public function setStatus($status);
}