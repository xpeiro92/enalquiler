<?php

interface RotatableComponent
{
    public function getPointingTo();
    public function setPointingTo($pointingTo);
}