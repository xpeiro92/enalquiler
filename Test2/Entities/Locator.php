<?php

include_once "Component.php";
include_once "DisplaceableAndRotateableComponent.php";

class Locator implements component, DisplaceableAndRotateableComponent
{
    private $id;
    private $position;
    private $status;
    private $pointingTo;

    public function __construct( $id, array $position, $pointingTo = 90)
    {
        $this->id = $id;
        $this->setStatus("ON");
        $this->setPosition($position);
        $this->setPointingTo($pointingTo);
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPointingTo()
    {
        return $this->pointingTo;
    }

    /**
     * @param mixed $pointingTo
     */
    public function setPointingTo($pointingTo)
    {
        $this->pointingTo = $pointingTo % 360;
    }


}