<?php

include_once "Component.php";

class LinearMotor implements Component
{
    private $id;
    private $status;
    private $speed;

    public function __construct($id)
    {
        $this->id = $id;
        $this->setStatus("OK");
        $this->setSpeed(0);
    }

    public function getId()
    {
        // TODO: Implement getId() method.
        return $this->id;
    }

    public function getStatus()
    {
        // TODO: Implement getStatus() method.
        return $this->status;
    }

    public function setStatus( $status )
    {
        // TODO: Implement setStatus() method.
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param mixed $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }
}