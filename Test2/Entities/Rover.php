<?php

include_once "LinearMotor.php";
include_once "Locator.php";
include_once "Component.php";

class Rover
{
    private $components = array();

    public static function initDefaultRover()
    {
        return new self([new LinearMotor("displacementMotor"), new Locator("displacementLocator", [0,0], 90)]);
    }

    public function __construct($components)
    {
        $this->setComponents($components);
    }


    public function getComponents()
    {
        return $this->components;
    }

    public function getComponentById($id)
    {
        return $this->components[$id];
    }


    private function setComponents($components)
    {
        foreach ($components as $component)
        {
            if ($component instanceof Component)
            {
                $this->components[$component->getId()] = $component;
            }
        }
    }
}