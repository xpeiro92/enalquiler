<?php

/**
 * Created by PhpStorm.
 * User: ras
 * Date: 10/07/17
 * Time: 17:43
 */
class LogMessage
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $severity
     * @param $message
     */
    public function __invoke($severity, $message)
    {
        switch ($severity) {
            case 0:
                $method = 'set_error';
                break;
            case 1:
                $method = 'set_info';
                break;
        }

        $this->logger->$method(
            '--------------------------------------------------------------------------------'
        );
        $this->logger->$method('>> '.$message);
        $this->logger->$method(
            '--------------------------------------------------------------------------------'
        );
    }
}

