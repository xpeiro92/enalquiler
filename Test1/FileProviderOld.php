<?php

class FileProviderOld
{

    public function setLogger($logger)
    {
        // ...
    }

    // $uri, $lastModified = '', $requireFile = false, $toNfs = false
    public function getFile($trim, $strtotime, $true, $destination)
    {
        return [];
    }
}

class DAOFiles
{

    public function update($array, $string)
    {
        // ...
    }
}

class Logger
{

    public function set_error($message)
    {
        echo "ERR >> ".$message.PHP_EOL;
    }

    public function set_info($message)
    {
        echo "INFO >> ".$message.PHP_EOL;
    }
}

class DownloadException extends Exception
{

}

class FileDownloader
{

    private $defaultValues;

    private $logger;

    private $downloadConfig;

    private $DOWNLOAD_ATTEMPTS;

    public function __construct(
        $defaultValues,
        $logger,
        $downloadConfig,
        $DOWNLOAD_ATTEMPTS
    ) {
        $this->defaultValues = $defaultValues;
        $this->logger = $logger;
        $this->downloadConfig = $downloadConfig;
    }

    protected function downloadFile($file, $destination)
    {
        $provider = new FileProvider();
        if (isset($this->defaultValues['logFileOperations'])
            && $this->defaultValues['logFileOperations']
        ) {
            $provider->setLogger($this->logger);
        }
        $attempts = 0;
        do {
            $this->logMessage(
                1,
                'Download attempt '.++$attempts.' of file '.$file['fichero']
            );
            $fileInfo = $provider->getFile(
                trim($file['fichero']),
                strtotime($file['fecha_ultima_act']),
                true,
                $destination
            );
        } while ($fileInfo['status'] == 0 && $attempts < 3);
        if ($fileInfo['status'] == 0) {
            $this->logMessage(0, 'Failed download of file '.$file['fichero']);
            throw new DownloadException(
                'Failed download of file '.$file['fichero']
            );
        } elseif ($fileInfo['status'] == 1) {
            if (!$fileInfo['new'] && $fileInfo['lastModified'] > 0) {
                // Update fecha del fichero
                $daoFiles = new DAOFilesUpdate(
                    new PDO('mysql:dbname=testdb;host=127.0.0.1')
                );
                $daoFiles->update(
                    [
                        'fecha_ultima_act' => date(
                            'Y-m-d H:i:s',
                            $fileInfo['lastModified']
                        ),
                    ],
                    'fk_id_tbl_usuarios='
                    .$this->downloadConfig['fk_id_tbl_usuarios']
                    ." and fichero='".addslashes($fileInfo['uri'])."'"
                );
            }
            $file['fileInfo'] = $fileInfo;
            $explodedPath = explode('.', $fileInfo['path']);
            if (
                (isset($fileInfo['content_type'])
                    && 'application/x-gzip' === $fileInfo['content_type'])
                || (!isset($fileInfo['content_type'])
                    && 'gz' === array_pop(
                        $explodedPath
                    ))
            ) {
                $file['fileInfo'] = $this->decompressFile($fileInfo);
            }
            $this->logMessage(
                1,
                'Completed download of file '.$file['fichero']
            );
        }

        return $file;
    }

    private function decompressFile($fileInfo)
    {
        // Decompression ...
        return $fileInfo;
    }

    private function logMessage($severity, $message)
    {
        switch ($severity) {
            case 0:
                $method = 'set_error';
                break;
            case 1:
                $method = 'set_info';
                break;
        }

        $this->logger->$method(
            '--------------------------------------------------------------------------------'
        );
        $this->logger->$method('>> '.$message);
        $this->logger->$method(
            '--------------------------------------------------------------------------------'
        );
    }
}