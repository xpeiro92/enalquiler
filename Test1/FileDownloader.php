<?php

/**
 * Created by PhpStorm.
 * User: ras
 * Date: 10/07/17
 * Time: 17:43
 */
class FileDownloader
{
    private $provider;
    private $downloadConfig;
    private $logMessage;
    private $decompressFile;
    private $daoFiles;
    private $content_type;


    public function __construct(
        FileProvider $provider,
        $downloadConfig,
        LogMessage $logMessage,
        DecompressFile $decompressFile,
        DAOFilesAction $daoFiles = null,
        $content_type
)
{
        $this->provider = $provider;
        $this->downloadConfig = $downloadConfig;
        $this->logMessage = $logMessage;
        $this->decompressFile = $decompressFile;
        $this->daoFiles = $daoFiles; //== null ? new DAOFilesUpdate(new PDO('mysql:dbname=testdb;host=127.0.0.1')) : $daoFiles;
        $this->content_type = $content_type;
    }

    protected function __invoke($file, $destination, $maxAttempts)
    {
        $fileInfo = $this->tryGetFile($maxAttempts, $destination, $file);

        if ($fileInfo['status'] == 0) {

            $this->logMessage(0, 'Failed download of file ' . $file['fichero']);
            throw new DownloadException(
                'Failed download of file ' . $file['fichero']
            );

        } elseif ($fileInfo['status'] == 1) {

            if (!$fileInfo['new'] && $fileInfo['lastModified'] > 0) {

                $this->daoFiles();/*(
                    [
                        'fecha_ultima_act' => date(
                            'Y-m-d H:i:s',
                            $fileInfo['lastModified']
                        ),
                    ],
                    'fk_id_tbl_usuarios=' . $this->downloadConfig['fk_id_tbl_usuarios'] . " and fichero='" . addslashes($fileInfo['uri']) . "'"
                );*/
            }

            $file = $this->descompressFile($fileInfo);
        }

        return $file;
    }

    private function tryGetFile($maxAttemps = 3, $destination, $file)
    {
        $attempts = 0;

        do {

            $this->logMessage(
                1,
                'Download attempt ' . ++$attempts . ' of file ' . $file['fichero']
            );

            $fileInfo = $this->provider->getFile(
                trim($file['fichero']),
                strtotime($file['fecha_ultima_act']),
                true,
                $destination
            );

        } while ($fileInfo['status'] == 0 && $attempts < $maxAttemps);

        return $fileInfo;
    }

    private function descompressFile($fileInfo)
    {
        $file['fileInfo'] = $fileInfo;
        $explodedPath = explode('.', $fileInfo['path']);

        if
        (
            ( isset($fileInfo['content_type']) && $this->content_type === $fileInfo['content_type'] ) ||
            ( !isset($fileInfo['content_type']) && $this->content_type === array_pop($explodedPath) )
        )
        {
            $file['fileInfo'] = $this->decompressFile($fileInfo);
        }

        $this->logMessage(
            1,
            'Completed download of file '.$file['fichero']
        );

        return $fileInfo;
    }
}